<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<title>Code Snippet</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/select2-bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/select2.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/select2.js"></script>
	<script type="text/javascript" src="js/custom.js"></script>

	<!-- HILIGHTED CODE -->
	<script src="highlighted/highlight.js" type="text/javascript"></script>
	<script src="highlighted/stringstream.js" type="text/javascript"></script>
	<script src="highlighted/tokenize.js" type="text/javascript"></script>
	<script src="highlighted/tokenizejavascript.js" type="text/javascript"></script>
	<script src="highlighted/parsejavascript.js" type="text/javascript"></script>
	<script src="highlighted/custom.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="highlighted/jscolors.css">
	<!-- HILIGHTED CODE -->
</head>
<body style="padding-top: 70px;padding-bottom: 0px;">
	<?php
	session_start();
	require_once("vendor/autoload.php");
	use src\snip_code\snip_code;
	use src\search\search;
	use src\comment\comment;
	use src\utilities;
	use src\header;
	// $_SESSION['user_id'] = 8;

	utilities::connect();
	if ((isset($_POST) && !empty($_POST)) && !isset($_SESSION['user_id'])) {
		$_SESSION['last_uri'] = $_SERVER['REQUEST_URI'];
		utilities::redirect("login.php");
	}
	if (isset($_GET['logout'])) {
		utilities::logout();
	}
	$header = new header();
	?>
	<div class="container">
		<div class="row">
		<?php
		if ((isset($_GET['view']) && $_GET['view'] != "") && (isset($_GET['action']) && $_GET['action'] != "")) {
			$file_name = "view/".$_GET['view']."/".$_GET['action'].".php";
			if (file_exists($file_name)) {
				include_once($file_name);
			} else {
				echo "File not found : <b>".$file_name."</b>";
			}
		} else {
			// echo "This is home page";
			include_once("view/search/index.php");
		}
		?>
		</div>
	</div>
	<?php
	####### MESSAGE VIEW ###################
	if ($message = utilities::flushMessage("success")) {
		?>
		<div class="alert alert-success alert-dismissible" style="top: 60px; right: 60px; position: absolute;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>SUCCESS: </strong><?php echo $message; ?></div>
		<?php
	}
	if ($message = utilities::flushMessage("error")) {
		?>
		<div class="alert alert-danger alert-dismissible" style="top: 60px; right: 60px; position: absolute;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>ERROR: </strong><?php echo $message; ?></div>
		<?php
	}
	####### MESSAGE VIEW ###################
	
	utilities::destroy();
	?>
	<script>
		$( ".select2" ).select2( { placeholder: "Select option", maximumSelectionSize: 6 } );

		$( ":checkbox" ).on( "click", function() {
			$( this ).parent().nextAll( "select" ).select2( "enable", this.checked );
		});

		$( "#demonstrations" ).select2( { placeholder: "Select2 version", minimumResultsForSearch: -1 } ).on( "change", function() {
			document.location = $( this ).find( ":selected" ).val();
		} );

		$( "button[data-select2-open]" ).click( function() {
			$( "#" + $( this ).data( "select2-open" ) ).select2( "open" );
		});
	</script>

	<script type="text/javascript">
	// var codeClass = $("div[class*='code']").length;
	$("div[class*='code']").each(function () {
	    var Id = this.id;
	    // alert(Id);
	    highlight(Id);
	});

	$('div[id^="commentRow_"]').each(function () {
	    var str = this.id;
	    var exploded = str.split('_');
	    var Id = exploded[1];
	    $("#commentBox_"+Id).addClass("panel-body");
	    // alert(Id);
	});
	</script>
	</body>
</html>
