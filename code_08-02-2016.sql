-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 08, 2016 at 04:09 AM
-- Server version: 5.5.34
-- PHP Version: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `code`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `user_id`, `title`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'PHP', 'PHP is a server side language', '2016-01-30 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 'ASP', 'ASP is server side language from microsoft', '2016-01-30 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 1, 'Javascript', 'Javascript is THE language', '2016-01-30 00:00:00', '2016-02-06 23:15:58', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `snip_code_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `snip_code_id`, `description`, `created_at`, `updated_at`) VALUES
(1, 2, 3, 'Hasan Sayeed Bappy', '2016-02-02 00:00:00', '2016-02-03 00:00:00'),
(2, 2, 3, 'Comment is final', '2016-02-02 00:00:00', '2016-02-03 00:00:00'),
(3, 2, 4, 'Eida kar photo', '2016-02-03 00:00:00', '0000-00-00 00:00:00'),
(4, 2, 4, 'Eida kar photo', '2016-02-03 00:00:00', '0000-00-00 00:00:00'),
(5, 2, 4, 'This is blank photo..', '2016-02-03 00:00:00', '2016-02-03 00:00:00'),
(8, 2, 7, 'Hello', '2016-02-03 00:00:00', '0000-00-00 00:00:00'),
(9, 2, 7, 'Hello asasa', '2016-02-03 00:00:00', '2016-02-03 00:00:00'),
(10, 2, 7, 'It''s good code', '2016-02-03 00:00:00', '0000-00-00 00:00:00'),
(11, 2, 7, 'yea its really good code\r\n', '2016-02-03 00:00:00', '0000-00-00 00:00:00'),
(14, 2, 7, 'good', '2016-02-03 00:00:00', '0000-00-00 00:00:00'),
(16, 8, 1, 'hellow\r\n', '2016-02-06 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE IF NOT EXISTS `likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `snip_code_id` int(11) NOT NULL,
  `is_like` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `user_id`, `snip_code_id`, `is_like`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 2, 7, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 2, 6, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 2, 8, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 2, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 8, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 8, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 3, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 1, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 4, 8, 1, '2016-02-07 11:23:53', '0000-00-00 00:00:00'),
(11, 4, 9, 1, '2016-02-07 11:24:39', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `personal_details`
--

CREATE TABLE IF NOT EXISTS `personal_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `present_address` varchar(255) NOT NULL,
  `permanent_address` varchar(255) NOT NULL,
  `mobile` int(11) NOT NULL,
  `alternative_email` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `img_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `personal_details`
--

INSERT INTO `personal_details` (`id`, `user_id`, `name`, `date_of_birth`, `gender`, `nationality`, `present_address`, `permanent_address`, `mobile`, `alternative_email`, `created_at`, `updated_at`, `deleted_at`, `img_name`) VALUES
(3, 1, 'Hasan Sayeed', '1991-01-03', 'Male', 'bangladeshi', 'Dhaka', 'Dhaka', 1912633787, 'jbhasan@gmailc.om', '2016-02-07 00:18:16', '2016-02-07 00:26:53', '0000-00-00 00:00:00', ''),
(4, 4, 'Md. Hasan Sayeed', '1990-01-03', 'Male', 'bangladeshi', 'Jhalakathi', 'Madartek, Basaboo', 1912633787, 'jb_hasan@live.com', '2016-02-07 11:27:53', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `shares`
--

CREATE TABLE IF NOT EXISTS `shares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `snip_code_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `shares`
--

INSERT INTO `shares` (`id`, `user_id`, `snip_code_id`, `created_at`, `deleted_at`) VALUES
(1, 2, 7, '2016-02-03 00:00:00', '0000-00-00 00:00:00'),
(2, 2, 1, '2016-02-06 00:00:00', '0000-00-00 00:00:00'),
(3, 2, 6, '2016-02-04 00:00:00', '0000-00-00 00:00:00'),
(4, 2, 8, '2016-02-04 00:00:00', '0000-00-00 00:00:00'),
(5, 2, 4, '2016-02-05 00:00:00', '0000-00-00 00:00:00'),
(6, 8, 4, '2016-02-06 00:00:00', '0000-00-00 00:00:00'),
(7, 8, 1, '2016-02-06 00:00:00', '0000-00-00 00:00:00'),
(8, 3, 1, '2016-02-06 00:00:00', '0000-00-00 00:00:00'),
(10, 1, 8, '2016-02-07 10:49:27', '0000-00-00 00:00:00'),
(11, 4, 8, '2016-02-07 11:25:58', '2016-02-07 11:26:38'),
(12, 4, 9, '2016-02-07 11:25:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `snip_code`
--

CREATE TABLE IF NOT EXISTS `snip_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `tag_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `snip_code`
--

INSERT INTO `snip_code` (`id`, `user_id`, `category_id`, `tag_id`, `title`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'a:3:{i:0;s:1:"1";i:1;s:1:"2";i:2;s:1:"4";}', 'Become a part of the world', 'This is code: <<error_reporting(0);>>\r\n<<function abc($a) {\r\n$a = 5;\r\n}>>\r\nSayeed\r\nThis function used for\r\n<<function abc($a) {\r\n$a = 5;\r\n}>>\r\nsecode function', '2016-01-31 00:00:00', '2016-02-01 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 1, 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Become a part of the world', 'This is code: <<error_reporting(0);>>\n<<function abc($a) {\n$a = 5;\n}>>\nSayeed\nThis function used for\n<<function abc($a) {\n$a = 5;\n}>>\nsecode function', '2016-01-31 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 1, 1, 'a:2:{i:0;s:1:"1";i:1;s:1:"3";}', '1-Our company served over 24121012 children', 'This is code: <<error_reporting(0);>>\r\n<<function abc($a) {\r\n$a = 5;\r\n}>>\r\nSayeed\r\nThis function used for\r\n<<function abc($a) {\r\n$a = 5;\r\n}>>\r\nsecode function', '2016-01-31 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 1, 1, 'a:2:{i:0;s:1:"1";i:1;s:1:"3";}', 'Our company served over 24121012 children', 'This is code: <<error_reporting(0);>>\r\n<<function abc($a) {\r\n$a = 5;\r\n}>>\r\nSayeed\r\nThis function used for\r\n<<function abc($a) {\r\n$a = 5;\r\n}>>\r\nsecode function\r\n<<echo "this is new code line";>>', '2016-01-31 00:00:00', '2016-02-01 00:00:00', '0000-00-00 00:00:00'),
(6, 1, 1, 'a:2:{i:0;s:1:"1";i:1;s:1:"3";}', 'Test Code', 'It is html code\r\n<<\r\nfunction abc($ab) {\r\nreturn false;\r\n}\r\n// comments here\r\n>>\r\n\r\nthats it', '2016-02-01 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 2, 1, 'a:1:{i:0;s:1:"1";}', 'Test PHP', 'this is php test\r\n<<echo "hasan";\r\nvar_dump($a);\r\nfunction abc($a) {\r\nreturn false;\r\n}\r\n>>', '2016-02-04 00:00:00', '2016-02-04 00:00:00', '0000-00-00 00:00:00'),
(9, 4, 1, 'a:2:{i:0;s:1:"1";i:1;s:1:"3";}', 'how to write a function', '<<function abc($a) {\r\nreturn false;\r\n}>>', '2016-02-07 11:24:33', '2016-02-07 11:24:55', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `user_id`, `title`, `created_at`) VALUES
(1, 1, 'PHP', '2016-02-06 00:00:00'),
(2, 1, 'HTML', '2016-02-01 00:00:00'),
(3, 1, 'Function', '2016-02-02 00:00:00'),
(4, 2, 'Classs', '2016-02-04 00:00:00'),
(5, 1, 'OOP', '2016-02-06 22:57:10'),
(7, 1, 'Angular JS', '2016-02-06 23:04:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `is_admin` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`user_name`,`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `password`, `email`, `is_admin`, `created_at`) VALUES
(1, 'hasan', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'jbhasan@gmail.com', 1, '2016-02-06 16:49:15'),
(4, 'sayeed', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'jbhasan98@gmail.com', 0, '2016-02-06 23:28:28');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
