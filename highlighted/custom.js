function highlight(code) {
	var codeFrom = document.getElementById(code).innerHTML;
	var output = document.getElementById(code);
	output.innerHTML = "";
	var newPRE = document.createElement("pre");
	output.appendChild(newPRE);

	function addLine(line) {
		for (var i = 0; i < line.length; i++) newPRE.appendChild(line[i]);
		newPRE.appendChild(document.createElement("BR"));
	}
	highlightText(codeFrom, addLine);
}