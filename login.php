<?php 
	session_start();
	require_once("vendor/autoload.php");
	use src\snip_code\snip_code;
	use src\search\search;
	use src\comment\comment;
	use src\users\users;
	use src\utilities;
	use src\header;

	utilities::connect();

	$users=new users;
	if (isset($_POST['btnRegistration'])) {
		$users->registration($_POST);
	}
	if (isset($_POST['btnLogin'])) {
		$users->login($_POST);
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<title>Code Snippet</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="css/select2-bootstrap.css">
		<link rel="stylesheet" type="text/css" href="css/select2.css">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
		<script type="text/javascript" src="js/select2.js"></script>
		<script type="text/javascript" src="js/custom.js"></script>

		<!-- HILIGHTED CODE -->
		<script src="highlighted/highlight.js" type="text/javascript"></script>
		<script src="highlighted/stringstream.js" type="text/javascript"></script>
		<script src="highlighted/tokenize.js" type="text/javascript"></script>
		<script src="highlighted/tokenizejavascript.js" type="text/javascript"></script>
		<script src="highlighted/parsejavascript.js" type="text/javascript"></script>
		<script src="highlighted/custom.js" type="text/javascript"></script>
		<link rel="stylesheet" type="text/css" href="highlighted/jscolors.css">
		<!-- HILIGHTED CODE -->
	</head>
	<body>
		<?php
		$header = new header();
		?>
		<div class="container">
			<div style="margin-top:70px;margin-bottom:50px" class="row">
				<div class="col-md-5">
					<form action="" method="post"> 
						<div class="modal-dialog">
							<div class="modal-content panel-info">
								<div class="modal-header panel-heading"><h4>REGISTRATION <small><i>new user</i></small></h4></div>
								<div class="modal-body">
									<div class="form-group">
										<label for="user_name">User Name :</label>
										<input type="text" name="user_name" class="form-control" value="" id="user_name" placeholder="Full name *" required />
									</div>
									<div class="form-group">
										<label for="email">Email:</label> 
										<input type="text" name="email" class="form-control" value="" id="email" placeholder="Email *" required/>
									</div>
									<div class="form-group" > 
										<label for="password1" >Password:</label>
										<input type="password" name="password" class="form-control" value="" id="password1" placeholder="Password *" required/>
									</div>
									<div class="form-group" >
										<label for="password2" >Confirm Password:</label>
										<input type="password" name="confirm_password" class="form-control" value="" id="password2" placeholder="Confirm Password *" required />
									</div>
								</div>
								<div class="modal-footer">
									<input type="submit" class="btn btn-info" name="btnRegistration" value="Registration" />
									<input type="reset" class="btn btn-default" value="Reset" />  
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="col-md-2"></div>
				<div style="border:0px solid ;padding:40px" class="col-md-5"> 
					<form action="" method="post"> 
						<div class="modal-dialog modal-sm">
							<div class="modal-content panel-success">
								<div class="modal-header panel-heading"><h4>SIGN IN <small><i>existing user</i></small></h4></div>
								<div class="modal-body">
									<div class="form-group">
										<label for="login_email">Email :</label>
										<input type="text" name="email" class="form-control" value="" id="login_email" placeholder="Email *" required  />
									</div>
									<div class="form-group">
										<label for="login_password">Password:</label>
										<input type="password" name="password" class="form-control" value="" id="login_password" placeholder="Password *" required />
									</div>
								</div>
								<div class="modal-footer">
									<input type="submit" class="btn btn-success" name="btnLogin" value="Log in" />
									<input type="reset" class="btn btn-default" value="Reset" />
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php
		####### MESSAGE VIEW ###################
		if ($message = utilities::flushMessage("success")) {
			?>
			<div class="alert alert-success alert-dismissible" style="top: 60px; right: 60px; position: absolute;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>SUCCESS: </strong><?php echo $message; ?></div>
			<?php
		}
		if ($message = utilities::flushMessage("error")) {
			?>
			<div class="alert alert-danger alert-dismissible" style="top: 60px; right: 60px; position: absolute;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>ERROR: </strong><?php echo $message; ?></div>
			<?php
		}
		####### MESSAGE VIEW ###################
		?>
	</body>
</html>