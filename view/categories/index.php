<?php
use src\snip_code\snip_code;
use src\comment\comment;
use src\like\like;
use src\share\share;
use src\tags\tags;
use src\categories\categories;
use src\utilities;

if (isset($_POST['btnAdd'])) {
	$data = $_POST;
	categories::create($data);
}
if (isset($_POST['btnUpdate'])) {
	$data = $_POST;
	categories::update($data);
}

if (isset($_POST['btnEdit'])) {
	$requested_data = categories::index($_POST['edit_category_id']);
}
if (isset($_POST['btnDelete'])) {
	categories::delete($_POST['edit_category_id']);
}
if (isset($_POST['btnSearch'])) {
	$list = categories::index(false, $_POST['search']);
} else {
	$list = categories::index();
}
?>
<form class="form-inline pull-left" action="" method="post">
	<div class="form-group">
		<label for="title">Category Name</label>
		<input type="text" class="form-control" value="<?php echo isset($requested_data) ? utilities::searchSubArray($requested_data, 'title') : ""; ?>" id="title" name="title" placeholder="Enter Category" />
		<label for="description">Description</label>
		<input type="text" class="form-control" value="<?php echo isset($requested_data) ? utilities::searchSubArray($requested_data, 'description') : ""; ?>" id="description" name="description" placeholder="Enter Category" />
		<?php if(isset($requested_data)) { ?>
			<input type="hidden" value="<?php echo utilities::searchSubArray($requested_data, 'id'); ?>" id="column_id" name="column_id" />
			<input type="submit" class="btn btn-info" name="btnUpdate" value="Update Category" />
		<?php } else { ?>
			<input type="submit" class="btn btn-success" name="btnAdd" value="Add Category" />
		<?php } ?>
	</div>
</form>

<form class="form-inline pull-right" action="" method="post">
	<div class="form-group">
		<input type="text" class="form-control" value="<?php echo isset($_POST['search']) ? $_POST['search'] : ""; ?>" id="search" name="search" placeholder="Category search..." />
		<input type="submit" class="btn btn-default" name="btnSearch" value="Search" />
	</div>
</form>
<div class="clearfix"></div>
<hr />
<?php
if (is_array($list) && !empty($list)) {
	?>
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Description</th>
					<th>Created At</th>
					<th>Updated At</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$serial = 0;
				foreach ($list as $key => $row) {
					$serial++;
					?>
					<tr>
						<td><?php echo $serial; ?></td>
						<td><?php echo $row['title']; ?></td>
						<td><?php echo $row['description']; ?></td>
						<td><?php echo $row['created_at']; ?></td>
						<td><?php echo $row['updated_at']; ?></td>
						<td>
							<form action="" method="post">
								<input type="hidden" name="edit_category_id" value="<?php echo $row['id']; ?>" />
								<input type="submit" name="btnEdit" class="btn btn-warning" value="Edit" />
								<input type="submit" name="btnDelete" class="btn btn-danger" value="Delete" />
							</form>
						</td>
					</tr>
					<?php
				}
				?>
			</tbody>
		</table>
	</div>
	<?php
} else {
	?>
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		<pre>No data found</pre>
	</div>
	<?php
}