<?php
use src\snip_code\snip_code;
use src\comment\comment;
use src\like\like;
use src\share\share;
use src\tags\tags;
use src\users\users;
use src\profiles\profiles;
use src\utilities;

if (isset($_POST['btnSubmit'])) {
	$data = $_POST;
	profiles::create($data);
}

$list = profiles::index(false, $_SESSION['user_id']);
if (!empty($list)) {
	$profile_data = $list[$_SESSION['user_id']];
} else {
	$profile_data = NULL;
}
?>
<form action="" method="post" enctype="multipart/form-data">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			   	<h4 class="modal-title"><i class="fa fa-edit"></i> Edit Profile</h4>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<!-- Update Image-->
						<div class=" col-sm-6 col-md-3 updateImage">
							<div class="form-group">
								<label for="updateImage">Update Image</label>
								<input type="file" name="update_image_name" id="updateImage" />
								<div id="imagePreview" class="img-responsive">
									<img src="../public/profile_pic/<?php echo $_SESSION['user_id'] ?>" alt="<?php echo users::usernameById($_SESSION['user_id']); ?>" title="<?php echo users::usernameById($_SESSION['user_id']); ?>">
								</div>
							</div>
						</div>
						<!-- Your Name-->
						<div class="col-sm-6 col-md-3 updateName">
							<div class="form-group">
								<label for="name" class="control-label">Your Name:<span class="star">*</span></label>
								<input type="text" class="form-control" name="name" value="<?php echo $profile_data['name'] ?>" required>
							</div>
						</div>
					</div>
					<div class="row">
						<!-- Permanent Address-->
						<div class="col-sm-6 col-md-9 updateAddressField">
							<div class="form-group">
								<label for="permanent_address" class="control-label">Permanent Address:<span class="star">*</span></label>
								<input type="text" class="form-control" name="permanent_address" value="<?php echo $profile_data['permanent_address'] ?>" required>
							</div>
						</div>
					</div>
					<div class="row">
						<!-- Present Address-->
					   <div class=" col-sm-6 col-md-9 updateAddressField">
							<div class="form-group">
								<label for="present_address" class="control-label">Present Address:<span class="star">*</span></label>
								<input type="text" class="form-control" name="present_address" value="<?php echo $profile_data['present_address'] ?>" required>
							</div>
					   </div>
					</div>  
					<div class="row">
						<!-- About Me-->
						<div class="col-md-9">
							<div class="form-group">
								<label for="about_me" class="control-label">About Me:</label>
								<textarea class="form-control" rows="3" name="about_me" id="about_me" value=""><?php echo $profile_data['about_me'] ?></textarea>
							</div>
						</div>
					</div> 
					<div class="row">
						<!-- Nationality -->
						<div class="col-sm-6 col-md-3">
							<div class="form-group">
								<label for="nationality" class="control-label">Nationality:<span class="star">*</span></label>
								<input type="text" class="form-control" id="nationality" name="nationality" value="<?php echo $profile_data['nationality'] ?>" required>
							</div>
						</div>
						<!-- Mobile-->
						<div class="col-sm-6 col-md-3">
							<div class="form-group">
								<label for="mobile" class="control-label">Mobile:<span class="star">*</span></label>
								<input type="text" class="form-control" id="mobile" name="mobile" value="<?php echo $profile_data['mobile'] ?>" required>
							</div>
						</div>
						<!-- Alternative Email-->
						<div class="col-sm-6 col-md-3">
							<div class="form-group">
								<label for="alternative_email" class="control-label">Alternative Email:</label>
								<input type="email" class="form-control" id="alternative_email" name="alternative_email" value="<?php echo $profile_data['alternative_email'] ?>">
							</div>
						</div>
					</div>
					<div class="row">
						<!-- Date Of Birth-->
						<div class="col-sm-6 col-md-3">
							<div class="form-group">
								<label for="date_of_birth" class="control-label">Date of Birth:</label>
								<input type="date" class="form-control" id="date_of_birth" name="date_of_birth" value="<?php echo $profile_data['date_of_birth'] ?>">
							</div>
						</div>
						<!-- Radio Button for Gender-->
						<div class="col-sm-6 col-md-5 col-md-offset-1">
							<div class="form-group">
							  <label class="control-label">Gender:<span class="star">*</span></label>
								<div class="radio radio-primary">
								  <label>
									<input name="gender" id="option1" value="Male" <?php echo ($profile_data['gender']=='Male')?'checked':'' ?> type="radio">
									Male
								  </label>
								
								  <label>
									<input name="gender" id="option2" value="Female" <?php echo ($profile_data['gender']=='Female')?'checked':'' ?>  type="radio">
									Female
								  </label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-raised" data-dismiss="modal">Close</button>
				<input type="submit" class="btn btn-primary btn-raised" name="btnSubmit" value="Save changes" />
			</div>
		</div>
	</div>
</form>