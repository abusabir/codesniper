<?php
use src\snip_code\snip_code;
use src\comment\comment;
use src\like\like;
use src\share\share;
use src\utilities;
if (isset($_POST['btnDelete'])) {
	snip_code::delete($_POST['column_id']); // column_id is snip_code_id
}
if (isset($_POST['btnRemoveShare'])) {
	share::softDelete($_POST['column_id']); // column_id is snip_code_id
}
if (isset($_POST['btnAddComment'])) {
	unset($_POST['btnAddComment']);
	foreach ($_POST as $key => $comments) {
		$exploded_key = explode("_", $key);
		$snip_code_id = $exploded_key[1];
	}
	$data['snip_code_id'] = $snip_code_id;
	$data['description'] = $comments;
	comment::create($data);
}
if (isset($_POST['btnUpdateComment'])) {
	$data['comment_id'] = $_POST['comment_id'];
	unset($_POST['btnUpdateComment']);
	unset($_POST['comment_id']);
	foreach ($_POST as $key => $comments) {
		$exploded_key = explode("_", $key);
		$snip_code_id = $exploded_key[1];
	}
	$data['description'] = $comments;
	comment::update($data);
}
if (isset($_POST['del_comment_id'])) {
	comment::delete($_POST['del_comment_id']);
}

if (isset($_POST['snip_code_id_for_like']) && $_POST['snip_code_id_for_like'] > 0) {
	$data['snip_code_id'] = $_POST['snip_code_id_for_like'];
	$data['is_like'] = $_POST['is_like'];
	like::create($data);
}
if (isset($_POST['snip_code_id_for_share']) && $_POST['snip_code_id_for_share'] > 0) {
	$data['snip_code_id'] = $_POST['snip_code_id_for_share'];
	share::create($data);
}

$shared_snip_code_ids = share::index(false, $_SESSION['user_id']);
utilities::$tmp_value = array();
$shared_snip_code_ids = implode(", ", array_unique(array_values(utilities::GetValueSearchSubArray($shared_snip_code_ids, "snip_code_id"))));
$list = snip_code::index(false, $_SESSION['user_id'], $shared_snip_code_ids);
if (is_array($list) && !empty($list)) {
	?>
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		<?php
		if (isset($snip_code_id)) {
			$snip_code_id = $snip_code_id;
		} else {
			$snip_code_id = false;
		}
		if (isset($_REQUEST['search']) && $_REQUEST['search'] != "") {
			$search_word = $_REQUEST['search'];
		} else {
			$search_word = false;
		}
		snip_code::snip_code_footer($list, $snip_code_id, $search_word);
		?>
	</div>
	<?php
}