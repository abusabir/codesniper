<?php
use src\snip_code\snip_code;
use src\comment\comment;
use src\utilities;
$class = new snip_code();

if (isset($_REQUEST['view_id'])) {
	$_REQUEST['column_id'] = $_REQUEST['view_id'];
}
if (isset($_POST['btnAddComment'])) {
	unset($_POST['btnAddComment']);
	foreach ($_POST as $key => $comments) {
		$exploded_key = explode("_", $key);
		$snip_code_id = $exploded_key[1];
	}
	$data['snip_code_id'] = $snip_code_id;
	$data['description'] = $comments;
	comment::create($data);
}
if (isset($_POST['btnUpdateComment'])) {
	$data['comment_id'] = $_POST['comment_id'];
	unset($_POST['btnUpdateComment']);
	unset($_POST['comment_id']);
	foreach ($_POST as $key => $comments) {
		$exploded_key = explode("_", $key);
		$snip_code_id = $exploded_key[1];
	}
	$data['description'] = $comments;
	comment::update($data);
}
if (isset($_REQUEST['column_id'])) {
	$list = $class->index($_REQUEST['column_id']);
	if (is_array($list) && !empty($list)) {
		$serial = 0;
		foreach ($list as $key => $row) {
			$serial++;
			?>
			<div class="row">
				<?php
				if (isset($_REQUEST['search']) && $_REQUEST['search'] != "") {
					$search_word = $_REQUEST['search'];
				} else {
					$search_word = false;
				}
				snip_code::snip_code_footer($list, $row['id'], $search_word);
				?>
			</div>
			<?php
		}
	}
}