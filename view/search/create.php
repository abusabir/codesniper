<?php
use src\snip_code\snip_code;
use src\categories\categories;
use src\tags\tags;
use src\utilities;
if (isset($_POST['btnSubmitData'])) {
	$_POST['tag_id'] = serialize($_POST['tag_id']);
	$data = array_merge($_POST, $_FILES);
	if(snip_code::create($data)) {
		?><script type="text/javascript">open("?view=snip_code&action=index", "_self");</script><?php
	}
}
?>
<br>
<div class="col-md-5">
	<form class="form-group" action="" method="post" enctype="multipart/form-data">
		<div class="form-group">
			<label for="category_id">Category</label>
			<select id="category_id" class="form-control" name="category_id" required>
				<option value="">Select Category</option>
				<?php
				$categories = categories::index();
				foreach ($categories as $key => $value) {
					?>
					<option value="<?php echo $value['id']; ?>"><?php echo $value['title']; ?></option>
					<?php
				}
				?>
			</select>
		</div>
		<div class="form-group">
			<label for="title">Title</label>
			<input type="title" class="form-control" id="title" name="title" placeholder="Title">
		</div>
		<div class="form-group">
			<label for="description">Description</label>
			<textarea id="description" cols="100" rows="15" name="description" class="form-control" placeholder="Description"></textarea>
			<p class="help-block">use "ctrl+y" for write code</p>
		</div>
		<div class="form-group">
			<label for="attachment">Attachment Image</label>
			<input type="file" id="attachment" accept="image/jpeg, image/png, image/gif" name="attachment[]" class="hidden" />
			<label for="attachment"><img src="imgs/picture.png" style="width:50px;"></label>
		</div>
		<div class="form-group">
			<label for="tag_id">Tags</label>
			<select id="tag_id" class="form-control select2 select2-offscreen" multiple="multiple" name="tag_id[]" required>
				<option value=""></option>
				<?php
				$tags = tags::index();
				foreach ($tags as $key => $value) {
					?>
					<option value="<?php echo $value['id']; ?>"><?php echo $value['title']; ?></option>
					<?php
				}
				?>
			</select>
		</div>
		<input type="submit" class="btn btn-default" value="Submit" name="btnSubmitData">
	</form>
</div>


<script type="text/javascript">
	$('#description').keydown(function (e) {
	  if (e.ctrlKey && e.keyCode == 89) { // for ctrl+y _____ //
	    var content = $('#description').val();
	    var position = $("#description").getCursorPosition();
	    //alert(content.substr(0, position));
	    var newContent = content.substr(0, position) + "<<>>" + content.substr(position);
	    $('#description').val(newContent);
	    setCaretToPos($("#description")[0], position+2);
	  }
	});

	(function ($, undefined) {
	    $.fn.getCursorPosition = function() {
	        var el = $(this).get(0);
	        var pos = 0;
	        if('selectionStart' in el) {
	            pos = el.selectionStart;
	        } else if('selection' in document) {
	            el.focus();
	            var Sel = document.selection.createRange();
	            var SelLength = document.selection.createRange().text.length;
	            Sel.moveStart('character', -el.value.length);
	            pos = Sel.text.length - SelLength;
	        }
	        return pos;
	    };
	})(jQuery);

	function setSelectionRange(input, selectionStart, selectionEnd) {
	  if (input.setSelectionRange) {
	    input.focus();
	    input.setSelectionRange(selectionStart, selectionEnd);
	  } else if (input.createTextRange) {
	    var range = input.createTextRange();
	    range.collapse(true);
	    range.moveEnd('character', selectionEnd);
	    range.moveStart('character', selectionStart);
	    range.select();
	  }
	}

	function setCaretToPos(input, pos) {
	  setSelectionRange(input, pos, pos);
	}
</script>