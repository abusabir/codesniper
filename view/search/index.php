<?php
use src\snip_code\snip_code;
use src\search\search;
use src\comment\comment;
use src\like\like;
use src\share\share;
use src\utilities;

if (isset($_POST['btnDelete'])) {
	search::delete($_POST['column_id']);
}
if (isset($_POST['btnRemoveShare'])) {
	share::softDelete($_POST['column_id']); // column_id is snip_code_id
}
if (isset($_POST['btnAddComment'])) {
	unset($_POST['btnAddComment']);
	foreach ($_POST as $key => $comments) {
		$exploded_key = explode("_", $key);
		$snip_code_id = $exploded_key[1];
	}
	$data['snip_code_id'] = $snip_code_id;
	$data['description'] = $comments;
	comment::create($data);
}
if (isset($_POST['btnUpdateComment'])) {
	$data['comment_id'] = $_POST['comment_id'];
	unset($_POST['btnUpdateComment']);
	unset($_POST['comment_id']);
	foreach ($_POST as $key => $comments) {
		$exploded_key = explode("_", $key);
		$snip_code_id = $exploded_key[1];
	}
	$data['description'] = $comments;
	comment::update($data);
}
if (isset($_POST['del_comment_id'])) {
	comment::delete($_POST['del_comment_id']);
}

if (isset($_POST['snip_code_id_for_like']) && $_POST['snip_code_id_for_like'] > 0) {
	$data['snip_code_id'] = $_POST['snip_code_id_for_like'];
	$data['is_like'] = $_POST['is_like'];
	like::create($data);
}
if (isset($_POST['snip_code_id_for_share']) && $_POST['snip_code_id_for_share'] > 0) {
	$data['snip_code_id'] = $_POST['snip_code_id_for_share'];
	share::create($data);
}

$data_list = array();
if (isset($_REQUEST['search']) && $_REQUEST['search'] != "") {
	$search = $_REQUEST['search'];
	$search_array = explode(" ", $search);
	foreach ($search_array as $key => $each_search_text) {
		$list = search::search($each_search_text);
		$data_list = array_merge($data_list, $list);
	}
} else {
	$data_list = snip_code::index(false, false, false, 5);
}
if (is_array($data_list) && !empty($data_list)) {
	?>
	<div class="row">
		<div class="col-md-12">
			<form class="form-inline" action="" method="get">
				<div class="col-md-2 text-right" style="padding-top: 7px; padding-right: 0px;">
					<label for="search">Search</label>
				</div>
				<div class="col-md-7">
					<input type="text" style="width:100%" value="<?php echo isset($_REQUEST['search']) ? $_REQUEST['search'] : ""; ?>" id="search" name="search" class="form-control" placeholder="Search...">
				</div>
			</form>
		</div>
	</div>
	<div class="clear">&nbsp;</div>

	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		<?php
		if (isset($snip_code_id)) {
			$snip_code_id = $snip_code_id;
		} else {
			$snip_code_id = false;
		}
		if (isset($_REQUEST['search']) && $_REQUEST['search'] != "") {
			$search_word = $_REQUEST['search'];
		} else {
			$search_word = false;
		}
		snip_code::snip_code_footer($data_list, $snip_code_id, $search_word);
		?>
	</div>
	<?php
} else {
	?>
	<div class="row">
		<div class="col-md-12">
			<form class="form-inline" action="" method="get">
				<div class="col-md-2 text-right" style="padding-top: 7px; padding-right: 0px;">
					<label for="search">Search</label>
				</div>
				<div class="col-md-7">
					<input type="text" style="width:100%" value="<?php echo isset($_REQUEST['search']) ? $_REQUEST['search'] : ""; ?>" id="search" name="search" class="form-control" placeholder="Search...">
				</div>
			</form>
		</div>
	</div>
	<?php
}
?>