<?php
use src\snip_code\snip_code;
use src\comment\comment;
use src\like\like;
use src\share\share;
use src\tags\tags;
use src\utilities;

if (isset($_POST['btnAdd'])) {
	$data = $_POST;
	tags::create($data);
}
if (isset($_POST['btnUpdate'])) {
	$data = $_POST;
	tags::update($data);
}

if (isset($_POST['btnEdit'])) {
	$requested_data = tags::index($_POST['edit_tag_id']);
}
if (isset($_POST['btnDelete'])) {
	tags::delete($_POST['edit_tag_id']);
}
if (isset($_POST['btnSearch'])) {
	$list = tags::index(false, $_POST['search']);
} else {
	$list = tags::index();
}
?>
<form class="form-inline pull-left" action="" method="post">
	<div class="form-group">
		<label for="title">Tag</label>
		<input type="text" class="form-control" value="<?php echo isset($requested_data) ? utilities::searchSubArray($requested_data, 'title') : ""; ?>" id="title" name="title" placeholder="Enter tag" />
		<?php if(isset($requested_data)) { ?>
			<input type="hidden" value="<?php echo utilities::searchSubArray($requested_data, 'id'); ?>" id="column_id" name="column_id" />
			<input type="submit" class="btn btn-info" name="btnUpdate" value="Update Tag" />
		<?php } else { ?>
			<input type="submit" class="btn btn-success" name="btnAdd" value="Add Tag" />
		<?php } ?>
	</div>
</form>

<form class="form-inline pull-right" action="" method="post">
	<div class="form-group">
		<input type="text" class="form-control" value="<?php echo isset($_POST['search']) ? $_POST['search'] : ""; ?>" id="search" name="search" placeholder="Tag search..." />
		<input type="submit" class="btn btn-default" name="btnSearch" value="Search" />
	</div>
</form>
<div class="clearfix"></div>
<hr />
<?php
if (is_array($list) && !empty($list)) {
	?>
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Created At</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$serial = 0;
				foreach ($list as $key => $row) {
					$serial++;
					?>
					<tr>
						<td><?php echo $serial; ?></td>
						<td><?php echo $row['title']; ?></td>
						<td><?php echo $row['created_at']; ?></td>
						<td>
							<form action="" method="post">
								<input type="hidden" name="edit_tag_id" value="<?php echo $row['id']; ?>" />
								<input type="submit" name="btnEdit" class="btn btn-warning" value="Edit" />
								<input type="submit" name="btnDelete" class="btn btn-danger" value="Delete" />
							</form>
						</td>
					</tr>
					<?php
				}
				?>
			</tbody>
		</table>
	</div>
	<?php
} else {
	?>
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		<pre>No data found</pre>
	</div>
	<?php
}