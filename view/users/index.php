<?php
use src\snip_code\snip_code;
use src\comment\comment;
use src\like\like;
use src\share\share;
use src\tags\tags;
use src\users\users;
use src\utilities;

if (isset($_POST['btnRegistration'])) {
	$data = $_POST;
	users::registration($data);
}
if (isset($_POST['btnUpdate'])) {
	$data = $_POST;
	users::update($data);
}

if (isset($_POST['btnEdit'])) {
	$requested_data = users::index($_POST['edit_user_id']);
}
if (isset($_POST['btnDelete'])) {
	users::delete($_POST['edit_user_id']);
}
if (isset($_POST['btnSearch'])) {
	$list = users::index(false, $_POST['search']);
} else {
	$list = users::index();
}
?>
<form action="" method="post">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="user_name">User Name :</label>
				<input type="text" name="user_name" class="form-control" value="" id="user_name" placeholder="Full name *" required />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="email">Email:</label> 
				<input type="text" name="email" class="form-control" value="" id="email" placeholder="Email *" required/>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group" > 
				<label for="password1" >Password:</label>
				<input type="password" name="password" class="form-control" value="" id="password1" placeholder="Password *" required/>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group" >
				<label for="password2" >Confirm Password:</label>
				<input type="password" name="confirm_password" class="form-control" value="" id="password2" placeholder="Confirm Password *" required />
			</div>
		</div>
	</div>
	<input type="checkbox" id="is_admin" name="is_admin" value="1" /><label for="is_admin">&nbsp;Admin Privilege</label><br>
	<input type="submit" class="btn btn-primary" name="btnRegistration" value="Registration" />
	<input type="reset" class="btn btn-default" value="Reset" />  
</form>
<hr />

<form class="form-inline pull-right" action="" method="post">
	<div class="form-group">
		<input type="text" class="form-control" value="<?php echo isset($_POST['search']) ? $_POST['search'] : ""; ?>" id="search" name="search" placeholder="Username search..." />
		<input type="submit" class="btn btn-default" name="btnSearch" value="Search" />
	</div>
</form>
<div class="clearfix"></div>
<hr />
<?php
if (is_array($list) && !empty($list)) {
	?>
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th>Username</th>
					<th>Email</th>
					<th>Is Admin</th>
					<th>Created At</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$serial = 0;
				foreach ($list as $key => $row) {
					$serial++;
					?>
					<tr>
						<td><?php echo $serial; ?></td>
						<td><?php echo $row['user_name']; ?></td>
						<td><?php echo $row['email']; ?></td>
						<td><?php echo $row['is_admin']; ?></td>
						<td><?php echo $row['created_at']; ?></td>
						<td>
							<form action="" method="post">
								<input type="hidden" name="edit_user_id" value="<?php echo $row['id']; ?>" />
								<input type="submit" name="btnEdit" class="btn btn-warning" value="Edit" />
								<input type="submit" name="btnDelete" class="btn btn-danger" value="Delete" />
							</form>
						</td>
					</tr>
					<?php
				}
				?>
			</tbody>
		</table>
	</div>
	<?php
} else {
	?>
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		<pre>No data found</pre>
	</div>
	<?php
}