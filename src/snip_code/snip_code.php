<?php
namespace src\snip_code;
use src\utilities;
use src\comment\comment;
use src\like\like;
use src\share\share;
use src\users\users;
use PDO;
class snip_code {
	public static $table = "snip_code";
	public static $tmp_id = 0;
		
	public static function index($column_id = false, $user_id = false, $snip_code_ids = false, $data_limit = 50) {
		$query = "select * from ".self::$table." where 1=1";
		if ($column_id) {
			$query .= " and id=$column_id";
		}
		if ($user_id && $snip_code_ids) {
			$query .= " and (user_id=$user_id or id in($snip_code_ids))";
		} elseif ($user_id && !$snip_code_ids) {
			$query .= " and user_id=$user_id";
		}

		$query .= " order by id desc";
		$query .= " limit 0, $data_limit";
		// echo $query;
		$result = utilities::$db->query($query);
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$content[$row['id']] = $row;
		}
		if (isset($content)) {
			return $content;
		} else {
			return $content = array();
		}
	}
	
	public static function create($data = array()) {
		if (is_array($data) && !empty($data)) {
			$query = "insert into ".self::$table." (user_id, category_id, tag_id, title, description, created_at) values (".$_SESSION['user_id'].", ".utilities::$db->quote($data['category_id']).", ".utilities::$db->quote($data['tag_id']).", ".utilities::$db->quote($data['title']).", ".utilities::$db->quote($data['description']).", NOW())";
			if(utilities::$db->query($query)) {
				$lastInsertId = Utilities::$db->lastInsertId();
				if (isset($data['attachment']) && ($data['attachment']['error']==0 || array_sum($data['attachment']['error']) == 0)) {
					$snipped_code_photo_dir = "snipped_code_photo";
					if (!file_exists($snipped_code_photo_dir)) {
						mkdir($snipped_code_photo_dir, 0777, true);
					}
					for ($i=0; $i < count($data['attachment']['name']); $i++) { 
						$photo_path = pathinfo($data['attachment']['name'][$i]);
						$photo_extension = $photo_path['extension'];
						move_uploaded_file($data['attachment']['tmp_name'][$i], $snipped_code_photo_dir."/".$lastInsertId."_".($i+1).".".$photo_extension);
					}
				}
				$content = "Successfully stored";
				utilities::setMessage($content, "success");
				return $content;
			} else {
				$content = "Inserting problem";
				utilities::setMessage($content, "error");
				return false;
			}
		}
	}
	
	public static function update($data = array()) {
		if (is_array($data) && !empty($data)) {
			if(array_key_exists("column_id", $data)) {				
				$query = "update ".self::$table." set category_id = ".utilities::$db->quote($data['category_id']).", tag_id = ".utilities::$db->quote($data['tag_id']).", title = ".utilities::$db->quote($data['title']).", description = ".utilities::$db->quote($data['description']).", updated_at = NOW() where id=".$data['column_id'];
				if(utilities::$db->query($query)) {
					$lastInsertId = $data['column_id'];
					if (isset($data['attachment']) && ($data['attachment']['error']==0 || array_sum($data['attachment']['error']) == 0)) {
						$snipped_code_photo_dir = "snipped_code_photo";
						if (!file_exists($snipped_code_photo_dir)) {
							mkdir($snipped_code_photo_dir, 0777, true);
						}
						for ($i=0; $i < count($data['attachment']['name']); $i++) { 
							$photo_path = pathinfo($data['attachment']['name'][$i]);
							$photo_extension = $photo_path['extension'];
							move_uploaded_file($data['attachment']['tmp_name'][$i], $snipped_code_photo_dir."/".$lastInsertId."_".($i+1).".".$photo_extension);
						}
					}
					$content = "Successfully updated";
					utilities::setMessage($content, "success");
					return $content;
				} else {
					$content = "Updating problem";
					utilities::setMessage($content, "error");
					return false;
				}
			}
		}
	}
	
	public static function delete($column_id) {
		$query = "delete from ".self::$table." where id=$column_id";
		if(utilities::$db->query($query)) {
			$content = "Successfully deleted";
			utilities::setMessage($content, "error");
			return $content;
		} else {
			$content = "Deleting problem";	
			utilities::setMessage($content, "error");
			return $content;	
		}
	}

	public static function code_view($data) {
		$data = html_entity_decode($data);
		$tmp_data = explode("<<\r\n", $data);
		for ($tmp=1; $tmp < count($tmp_data); $tmp++) {
			self::$tmp_id++;
			$data = self::str_replace2("<<\r\n", '<div class="code" id="code'.self::$tmp_id.'">', $data, 1);
		}
		$tmp_data = explode("<<", $data);
		for ($tmp=1; $tmp < count($tmp_data); $tmp++) {
			self::$tmp_id++;
			$data = self::str_replace2("<<", '<div class="code" id="code'.self::$tmp_id.'">', $data, 1);
		}
		$data = str_replace(">>", '</div>', $data);
		$data = str_replace('\r\n', "\r\n", $data);
		return trim($data);
	}

	public static function str_replace2($find, $replacement, $subject, $limit = 0, &$count = 0){
		if ($limit == 0)
	    	return str_replace($find, $replacement, $subject, $count);
		$ptn = '/' . preg_quote($find,'/') . '/';
		return preg_replace($ptn, $replacement, $subject, $limit, $count);
	}

	public static function snip_code_footer($data_list = array(), $snip_code_id = false, $search_word = false) {
		$serial = 0;
		foreach ($data_list as $key => $row) {
			$serial++;
			if ($snip_code_id && $snip_code_id == $row['id']) {
				$is_collapsed = " in";
			} else {
				$is_collapsed = "";
			}
			$title = $row['title'];
			$description = self::code_view($row['description']);

			if ($search_word && $search_word != "") {
				$search_array = explode(" ", $search_word);
				foreach ($search_array as $key => $word) {
					$title = preg_replace ("/" . preg_quote($word) . "/",
					                        "<mark>" .$word . "</mark>",
					                        $title);
				}
			}
			?>
		 	<div class="panel panel-<?php echo isset($_SESSION['user_id']) && $row['user_id'] == $_SESSION['user_id'] ? "info" : "primary"; ?>">
				<div class="panel-heading" role="tab" id="heading<?php echo $row['id']; ?>" style="padding:0;">
					<h4 class="panel-title pull-left" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $row['id']; ?>" aria-expanded="true" aria-controls="collapse<?php echo $row['id']; ?>" style="cursor:pointer; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; line-height: normal; width: 75%; padding: 15px;"><?php echo $title; ?></h4>
					<?php
					if (!isset($_GET['action']) || $_GET['action'] != "view") {
						$check_shared = share::check_shared($row['id']);
						?>
						<div style="padding-top:8px;">
							<?php
							if (isset($_SESSION['user_id']) && $row['user_id'] == $_SESSION['user_id']) {
								?>
								<form class="pull-right" action="" method="post" style="margin-right:5px;">
									<input type="hidden" name="column_id" value="<?php echo $row['id']; ?>">
									<input type="submit" class="btn btn-danger" name="btnDelete" value="Delete">
								</form>
								<form action="?view=snip_code&action=edit&view_id=<?php echo $row['id']; ?>" method="post" class="pull-right" style="margin-right:5px;">
									<input type="hidden" name="column_id" value="<?php echo $row['id']; ?>">
									<input type="submit" class="btn btn-warning" name="btnEdit" value="Edit">
								</form>
								<?php
							} elseif(isset($_SESSION['user_id']) && $check_shared) {
								?>
								<form action="" method="post" class="pull-right" style="margin-right:5px;">
									<input type="hidden" name="column_id" value="<?php echo $row['id']; ?>">
									<input type="submit" class="btn btn-danger" name="btnRemoveShare" value="Remove">
								</form>
								<?php
							}
							?>
							<form action="" method="get" class="pull-right" style="margin-right:5px;">
								<input type="hidden" name="view" value="snip_code">
								<input type="hidden" name="action" value="view">
								<input type="hidden" name="view_id" value="<?php echo $row['id']; ?>">
								<button type="submit" class="btn btn-info" value="View">View</button>
							</form>
						</div>
						<?php
					}
					?>
					<div class="clearfix"></div>
				</div>
				<div id="collapse<?php echo $row['id']; ?>" class="panel-collapse collapse <?php echo $is_collapsed; ?>" role="tabpanel" aria-labelledby="heading<?php echo $row['id']; ?>">
					<div class="panel-body">
						<div id="description"><?php echo $description; ?></div>
						<?php
						$check_photo = glob("snipped_code_photo/".$row['id']."_*.*");
						foreach ($check_photo as $key => $value) {
							?><img src="<?php echo $value; ?>" class="img-rounded" style="border:solid 1px black;" /><?php 
						}
						?>
					</div>
					<div class="panel-footer">
						<div class="pull-left">
							<?php
							$comment_list = comment::index(false, false, $row['id']);
							if(count($comment_list)) {
								$new_comment_class = "text-primary";
							} else {
								$new_comment_class = "";
							}
							$like_count = like::like_count($row['id']);
							$share_count = share::share_count($row['id']);

							// check login
							if (isset($_SESSION['user_id']) && $_SESSION['user_id'] > 0) {
								$share_check = share::check_shared($row['id']);
								$is_liked = like::check_liked($row['id']);
							} else {
								$share_check = false;
								$is_liked = false;
							}
							// check login //

							if($is_liked) {
								$is_like = 0;
								$new_like_class = "text-primary";
							} else {
								$is_like = 1;
								$new_like_class = "";
							}
							if($share_check) {
								$new_share_class = "text-primary";
							} else {
								$new_share_class = "";
							}
							?>
							<form action="" method="post" class="pull-left">
								<input type="hidden" name="snip_code_id_for_like" value="<?php echo $row['id']; ?>">
								<input type="hidden" name="is_like" value="<?php echo $is_like; ?>">
								<button type="submit" class="btn btn-linkk"><span class="glyphicon glyphicon-thumbs-up <?php echo $new_like_class; ?>" style="cursor:pointer; padding:0 10px; word-spacing: -8px;" title="Like">&nbsp;Like&nbsp;<span class="badge"><?php echo $like_count; ?></span></span></button>
							</form>
							<form action="" method="post" class="pull-left">
								<input type="hidden" name="snip_code_id_for_share" value="<?php echo $row['id']; ?>">
								<button type="submit" class="btn btn-linkk"><span class="glyphicon glyphicon-share-alt <?php echo $new_share_class; ?>" style="cursor:pointer; padding:0 10px; word-spacing: -8px;" title="Share">&nbsp;Share&nbsp;<span class="badge"><?php echo $share_count; ?></span></span></button>
							</form>
							<form class="pull-left">
								<button type="submit" class="btn btn-linkk" onclick="commentBox(<?php echo $row['id']; ?>); return false;"><span class="glyphicon glyphicon-comment <?php echo $new_comment_class; ?>" style="cursor:pointer; padding:0 10px; word-spacing: -8px;" title="Share">&nbsp;Comment&nbsp;<span class="badge"><?php echo count($comment_list); ?></span></span></button>
							</form>
						</div>
						<div class="pull-right">Created by: <?php echo users::usernameById($row['user_id']); ?>&nbsp;|&nbsp;Created at: <?php echo $row['created_at']; ?></div>
						<div class="clearfix"></div>
					</div>
					<div>
						<div id="commentBox_<?php echo $row['id']; ?>">
							<?php
							if (!empty($comment_list)) {
								comment::comment_view($comment_list, $row['id']);
							}
							?>
						</div>
					</div>
				</div>
			</div>
			<?php
		}
	}
}
?>