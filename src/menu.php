<?php
namespace src;
/**
* Utilities Class
*/
class menu  { 
	public function header_menu() {
		?>
		<div class="navbar navbar-inverse navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="index.php">CODE SNIPPET</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<?php
					if (isset($_SESSION['user_id']) && $_SESSION['user_id'] > 0) {
						?><ul class="nav navbar-nav"><?php
							if (isset($_SESSION['is_admin']) && $_SESSION['is_admin'] == 1) {
								?>
								<li class="dropdown"><a class="dropdown-toggle" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" href="#">ADMIN TOOLS</a>
									<ul class="dropdown-menu">
										<li><a href="?view=categories&action=index">Category List</a></li>
										<li><a href="?view=tags&action=index">Tag List</a></li>
										<li><a href="?view=users&action=index">User List</a></li>
									</ul>
								</li>
								<?php
							}
							?>
							<li class="dropdown"><a class="dropdown-toggle" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" href="#">SNIP CODE</a>
							<?php $module_for = "snip_code"; ?>
								<ul class="dropdown-menu">
									<li><a href="?view=<?php echo $module_for; ?>&action=index">List View</a></li>
									<li><a href="?view=<?php echo $module_for; ?>&action=create">Add New</a></li>
								</ul>
							</li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li><a href="?view=profiles&action=index">PROFILE</a></li>
							<li><a href="?logout">LOGOUT</a></li>
						</ul>
							<?php
						}
						?>
				</div>
			</div>
		</div>
		<?php
	}
}
?>