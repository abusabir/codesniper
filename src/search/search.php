<?php
namespace src\search;
use src\utilities;
use PDO;
class search {
	public static $table_code = "snip_code";
	public static $tmp_id = 0;
		
	public static function index($column_id = false, $user_id = false) {
		$query = "select * from ".self::$table_code." where 1=1";
		if ($column_id) {
			$query .= " and id=$column_id";
		}
		if ($user_id) {
			$query .= " and user_id=$user_id";
		}
		$result = utilities::$db->query($query);
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$content[$row['id']] = $row;
		}
		if (isset($content)) {
			return $content;
		} else {
			return $content = array();
		}
	}

	public static function search($search_text) {
		$query = "select * from ".self::$table_code." where 1=1";
		if ($search_text) {
			$query .= " and (title like '%$search_text%' or description like '%$search_text%')";
		}
		$result = utilities::$db->query($query);
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$content[$row['id']] = $row;
		}
		if (isset($content)) {
			return $content;
		} else {
			return $content = array();
		}
	}
	
	public static function create($data = array()) {
		if (is_array($data) && !empty($data)) {
			$query = "insert into ".self::$table_code." (user_id, category_id, tag_id, title, description, created_at) values (".$_SESSION['user_id'].", ".utilities::$db->quote($data['category_id']).", ".utilities::$db->quote($data['tag_id']).", ".utilities::$db->quote($data['title']).", ".utilities::$db->quote($data['description']).", NOW())";
			if(utilities::$db->query($query)) {
				$lastInsertId = Utilities::$db->lastInsertId();
				if (isset($data['attachment']) && ($data['attachment']['error']==0 || array_sum($data['attachment']['error']) == 0)) {
					$snipped_code_photo_dir = "snipped_code_photo";
					if (!file_exists($snipped_code_photo_dir)) {
						mkdir($snipped_code_photo_dir, 0777, true);
					}
					for ($i=0; $i < count($data['attachment']['name']); $i++) { 
						$photo_path = pathinfo($data['attachment']['name'][$i]);
						$photo_extension = $photo_path['extension'];
						move_uploaded_file($data['attachment']['tmp_name'][$i], $snipped_code_photo_dir."/".$lastInsertId."_".($i+1).".".$photo_extension);
					}
				}
				$content = "Successfully stored";
				utilities::setMessage($content, "success");
				return $content;
			} else {
				$content = "Inserting problem";
				utilities::setMessage($content, "error");
				return false;
			}
		}
	}
	
	public static function update($data = array()) {
		if (is_array($data) && !empty($data)) {
			if(array_key_exists("column_id", $data)) {				
				$query = "update ".self::$table_code." set category_id = ".utilities::$db->quote($data['category_id']).", tag_id = ".utilities::$db->quote($data['tag_id']).", title = ".utilities::$db->quote($data['title']).", description = ".utilities::$db->quote($data['description']).", updated_at = NOW() where id=".$data['column_id'];
				if(utilities::$db->query($query)) {
					$lastInsertId = $data['column_id'];
					if (isset($data['attachment']) && ($data['attachment']['error']==0 || array_sum($data['attachment']['error']) == 0)) {
						$snipped_code_photo_dir = "snipped_code_photo";
						if (!file_exists($snipped_code_photo_dir)) {
							mkdir($snipped_code_photo_dir, 0777, true);
						}
						for ($i=0; $i < count($data['attachment']['name']); $i++) { 
							$photo_path = pathinfo($data['attachment']['name'][$i]);
							$photo_extension = $photo_path['extension'];
							move_uploaded_file($data['attachment']['tmp_name'][$i], $snipped_code_photo_dir."/".$lastInsertId."_".($i+1).".".$photo_extension);
						}
					}
					$content = "Successfully updated";
					utilities::setMessage($content, "success");
					return $content;
				} else {
					$content = "Updating problem";
					utilities::setMessage($content, "error");
					return false;
				}
			}
		}
	}
	
	public static function delete($column_id) {
		$query = "delete from ".self::$table_code." where id=$column_id";
		if(utilities::$db->query($query)) {
			$content = "Successfully deleted";
			utilities::setMessage($content, "error");
			return $content;
		} else {
			$content = "Deleting problem";	
			utilities::setMessage($content, "error");
			return $content;	
		}
	}

	public static function code_view($data) {
		$data = html_entity_decode($data);
		$tmp_data = explode("<<\r\n", $data);
		for ($tmp=1; $tmp < count($tmp_data); $tmp++) {
			self::$tmp_id++;
			$data = self::str_replace2("<<\r\n", '<div class="code" id="code'.self::$tmp_id.'">', $data, 1);
		}
		$tmp_data = explode("<<", $data);
		for ($tmp=1; $tmp < count($tmp_data); $tmp++) {
			self::$tmp_id++;
			$data = self::str_replace2("<<", '<div class="code" id="code'.self::$tmp_id.'">', $data, 1);
		}
		$data = str_replace(">>", '</div>', $data);
		$data = str_replace('\r\n', "\r\n", $data);
		return trim($data);
	}

	public static function str_replace2($find, $replacement, $subject, $limit = 0, &$count = 0){
		if ($limit == 0)
	    	return str_replace($find, $replacement, $subject, $count);
		$ptn = '/' . preg_quote($find,'/') . '/';
		return preg_replace($ptn, $replacement, $subject, $limit, $count);
	}
}
?>