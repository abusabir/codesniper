<?php
namespace src\like;
use src\utilities;
use PDO;
class like {
	public static $table = "likes";
	public static $tmp_id = 0;
		
	public static function index($column_id = false, $user_id = false, $snip_code_id = false) {
		$query = "select * from ".self::$table." where 1=1";
		if ($column_id) {
			$query .= " and id=$column_id";
		}
		if ($user_id) {
			$query .= " and user_id=$user_id";
		}
		if ($snip_code_id) {
			$query .= " and snip_code_id=$snip_code_id";
		}
		$result = utilities::$db->query($query);
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$content[$row['id']] = $row;
		}
		if (isset($content)) {
			return $content;
		} else {
			return $content = array();
		}
	}
	
	public static function create($data = array()) {
		if (is_array($data) && !empty($data)) {
			$check_query = "select * from ".self::$table." where user_id=".$_SESSION['user_id']." and snip_code_id=".utilities::$db->quote($data['snip_code_id']);
			if(utilities::$db->query($check_query)->rowCount() > 0) {
				$query = "update ".self::$table." set is_like=".utilities::$db->quote($data['is_like']).", updated_at=NOW() where snip_code_id = ".utilities::$db->quote($data['snip_code_id'])." and user_id=".$_SESSION['user_id'];
			} else {
				$query = "insert into ".self::$table." (user_id, snip_code_id, is_like, created_at) values (".$_SESSION['user_id'].", ".utilities::$db->quote($data['snip_code_id']).", ".utilities::$db->quote($data['is_like']).", NOW())";
			}
			// echo $query;
			if(utilities::$db->query($query)) {
				$content = "Successfully stored";
				utilities::setMessage($content, "success");
				return $content;
			} else {
				$content = "Inserting problem";
				utilities::setMessage($content, "error");
				return false;
			}
		}
	}
	public static function like_count($snip_code_id) {
		if ($snip_code_id != "") {
			$query = "select count(*) as total_like from ".self::$table." where snip_code_id = ".$snip_code_id." and is_like=1";
			if($result = utilities::$db->query($query)) {
				$row = $result->fetch(PDO::FETCH_ASSOC);
				return $row['total_like'];
			} else {
				return 0;
			}
		}
	}
	public static function check_liked($snip_code_id) {
		if ($snip_code_id != "") {
			$query = "select id from ".self::$table." where snip_code_id = ".$snip_code_id." and is_like=1 and user_id=".$_SESSION['user_id'];
			if(utilities::$db->query($query)->rowCount() > 0) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	public static function update($data = array()) {
		if (is_array($data) && !empty($data)) {
			if(array_key_exists("comment_id", $data)) {				
				$query = "update ".self::$table." set description = ".utilities::$db->quote($data['description']).", updated_at = NOW() where id=".$data['comment_id'];
				if(utilities::$db->query($query)) {
					$content = "Successfully updated";
					utilities::setMessage($content, "success");
					return $content;
				} else {
					$content = "Updating problem";
					utilities::setMessage($content, "error");
					return false;
				}
			}
		}
	}
	
	public static function delete($column_id) {
		$query = "delete from ".self::$table." where id=$column_id";
		if(utilities::$db->query($query)) {
			$content = "Successfully deleted";
			utilities::setMessage($content, "error");
			return $content;
		} else {
			$content = "Deleting problem";	
			utilities::setMessage($content, "error");
			return $content;	
		}
	}

	public static function code_view($data) {
		$data = html_entity_decode($data);
		$tmp_data = explode("<<\r\n", $data);
		for ($tmp=1; $tmp < count($tmp_data); $tmp++) {
			self::$tmp_id++;
			$data = self::str_replace2("<<\r\n", '<div class="code" id="code'.self::$tmp_id.'">', $data, 1);
		}
		$tmp_data = explode("<<", $data);
		for ($tmp=1; $tmp < count($tmp_data); $tmp++) {
			self::$tmp_id++;
			$data = self::str_replace2("<<", '<div class="code" id="code'.self::$tmp_id.'">', $data, 1);
		}
		$data = str_replace(">>", '</div>', $data);
		$data = str_replace('\r\n', "\r\n", $data);
		return trim($data);
	}

	public static function str_replace2($find, $replacement, $subject, $limit = 0, &$count = 0){
		if ($limit == 0)
	    	return str_replace($find, $replacement, $subject, $count);
		$ptn = '/' . preg_quote($find,'/') . '/';
		return preg_replace($ptn, $replacement, $subject, $limit, $count);
	}

	public static function comment_view($comment_list = array(), $snip_code_id) {
		foreach ($comment_list as $comment_id => $value_in_array) {
			?>
			<div id="commentRow_<?php echo $snip_code_id; ?>_1" class="row">
				<div class="col-md-1 text-right">
					<img src="imgs/picture.png" style="width:50px;" />
				</div>
				<div class="col-md-11">
					<strong><?php echo $value_in_array['user_id']; ?></strong>
					<span><?php echo $value_in_array['description']; ?></span><br>
					<small style="color:gray;"><span class="glyphicon glyphicon-pencil" style="cursor:pointer;" onclick="commentBox(<?php echo $snip_code_id; ?>, <?php echo $value_in_array['id']; ?>, '<?php echo $value_in_array['description']; ?>')"> Edit</span> Commented : <?php echo $value_in_array['created_at']; ?></small>
				</div>
			</div>
			<div class="clearfix"><hr></div>
			<?php
		}
	}
}
?>