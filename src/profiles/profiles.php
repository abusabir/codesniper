<?php
namespace src\profiles;
use src\utilities;
use PDO;
class profiles {
	public static $table = "personal_details";
		
	public static function index($column_id = false, $user_id = false) {
		$query = "select * from ".self::$table." where 1";
		if ($column_id) {
			$query .= " and id=$column_id";
		}
		if ($user_id) {
			$query .= " and user_id=$user_id";
		}
		$result = utilities::$db->query($query);
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$content[$row['user_id']] = $row;
		}
		if (isset($content)) {
			return $content;
		} else {
			return $content = array();
		}
	}
	

	public static function create($data = array()) {
		if (is_array($data) && !empty($data)) {
			$check_query = "select * from ".self::$table." where user_id=".$_SESSION['user_id'];
			if(utilities::$db->query($check_query)->rowCount() > 0) {
				$query = "update ".self::$table." set name = ".utilities::$db->quote($data['name']).", date_of_birth = ".utilities::$db->quote($data['date_of_birth']).", gender = ".utilities::$db->quote($data['gender']).", nationality = ".utilities::$db->quote($data['nationality']).", present_address = ".utilities::$db->quote($data['present_address']).", permanent_address = ".utilities::$db->quote($data['permanent_address']).", mobile = ".utilities::$db->quote($data['mobile']).", alternative_email = ".utilities::$db->quote($data['alternative_email']).", updated_at=NOW() where user_id=".$_SESSION['user_id'];
			} else {
				$query = "insert into ".self::$table." (user_id, name, date_of_birth, gender, nationality, present_address, permanent_address, mobile, alternative_email, created_at) values (".$_SESSION['user_id'].", ".utilities::$db->quote($data['name']).", ".utilities::$db->quote(date("Y-m-d", strtotime($data['date_of_birth']))).", ".utilities::$db->quote($data['gender']).", ".utilities::$db->quote($data['nationality']).", ".utilities::$db->quote($data['present_address']).", ".utilities::$db->quote($data['permanent_address']).", ".utilities::$db->quote($data['mobile']).", ".utilities::$db->quote($data['alternative_email']).", NOW())";
			}
			if(utilities::$db->query($query)) {
				$content = "Successfully updated";
				utilities::setMessage($content, "success");
				return $content;
			} else {
				$content = "Updating problem";
				utilities::setMessage($content, "error");
				return false;
			}
		}
	}
	// ##### No Need Now ###########################
	
	public static function delete($column_id) {
		$query = "delete from ".self::$table." where id=$column_id";
		if(utilities::$db->query($query)) {
			$content = "Successfully deleted";
			utilities::setMessage($content, "success");
			return $content;
		} else {
			$content = "Deleting problem";	
			utilities::setMessage($content, "error");
			return $content;	
		}
	}
	
	public static function registration($data = array()){
 		if (is_array($data) && !empty($data)) {
	 		$query = "select * from ".self::$table." where user_name=".utilities::$db->quote($data['user_name'])." or email=".utilities::$db->quote($data['email']);
	 		
	 		if(utilities::$db->query($query)->rowCount() > 0){
	 			utilities::setMessage("Username or Email already used", "error");
	 		} else{
	 			if (!isset($data['is_admin'])) {
	 				$data['is_admin'] = 0;
	 			}
	 			$query = "insert into ".self::$table." (user_name, email, password, is_admin, created_at) values (".utilities::$db->quote($data['user_name']).", ".utilities::$db->quote($data['email']).", '".sha1($data['password'])."', ".utilities::$db->quote($data['is_admin']).", NOW())";
	 			if(utilities::$db->query($query)){
	 				utilities::setMessage("registration Success ");
	 			} else{
	 				utilities::setMessage("Something wrong", "error");
	 			}
	 		}
	 	}
 	}

 	public static function login($data = array()){ 
 		$query = "select * from ".self::$table." where password='".sha1($data['password'])."' and (email=".utilities::$db->quote($data['email'])." or user_name=".utilities::$db->quote($data['email']).")";
 		if($row = utilities::$db->query($query)->fetch(PDO::FETCH_ASSOC)) {
 			$_SESSION['user_id'] = $row['id'];
 			$_SESSION['is_admin'] = $row['is_admin'];
 			utilities::setMessage("login success");
 			if (isset($_SESSION['last_uri'])) {
 				utilities::redirect($_SESSION['last_uri']);
 			} else {
 				utilities::redirect('index.php');
 			}
 		} else{
 			utilities::setMessage("Incorrect email/username or Password");
 		}	
 	}

 	public static function usernameById($user_id){ 
 		$query = "select user_name from ".self::$table." where id=".$user_id;
 		if($row = utilities::$db->query($query)->fetch(PDO::FETCH_ASSOC)) {
 			return $row['user_name'];
 		} else{
 			return "Not Found";
 		}	
 	}
}
?>