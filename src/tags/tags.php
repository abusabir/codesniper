<?php
namespace src\tags;
use src\utilities;
use PDO;
class tags {
	public static $table = "tags";
		
	public static function index($column_id = false, $tag_title = false) {
		$query = "select * from ".self::$table." where 1";
		if ($column_id) {
			$query .= " and id=$column_id";
		}
		if ($tag_title) {
			$query .= " and title='$tag_title'";
		}
		$result = utilities::$db->query($query);
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$content[$row['id']] = $row;
		}
		if (isset($content)) {
			return $content;
		} else {
			return $content = array();
		}
	}
	
	public static function create($data = array()) {
		if (is_array($data) && !empty($data)) {
			$query = "insert into ".self::$table." (user_id, title, created_at) values (".$_SESSION['user_id'].", ".utilities::$db->quote($data['title']).", NOW())";
			if(utilities::$db->query($query)) {
				$content = "Successfully stored";
				utilities::setMessage($content, "success");
				return $content;
			} else {
				$content = "Inserting problem";
				utilities::setMessage($content, "error");
				return $content;
			}
		}
	}
	
	public static function update($data = array()) {
		if (is_array($data) && !empty($data)) {
			if(array_key_exists("column_id", $data)) {
			$query = "update ".self::$table." set title = ".utilities::$db->quote($data['title'])." where id=".$data['column_id'];
				if(utilities::$db->query($query)) {
					$content = "Successfully updated";
					utilities::setMessage($content, "success");
					return $content;
				} else {
					$content = "Updating problem";
					utilities::setMessage($content, "error");
					return $content;
				}
			}
		}
	}
	
	public static function delete($column_id) {
		$query = "delete from ".self::$table." where id=$column_id";
		if(utilities::$db->query($query)) {
			$content = "Successfully deleted";
			utilities::setMessage($content, "error");
			return $content;
		} else {
			$content = "Deleting problem";	
			utilities::setMessage($content, "error");
			return $content;	
		}
	}
}
?>