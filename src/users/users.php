<?php
namespace src\users;
use src\utilities;
use PDO;
class users {
	public static $table = "users";
		
	public static function index($column_id = false, $user_name = false) {
		$query = "select * from ".self::$table." where 1";
		if ($column_id) {
			$query .= " and id=$column_id";
		}
		if ($user_name) {
			$query .= " and user_name like '$user_name%'";
		}
		$result = utilities::$db->query($query);
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$content[$row['id']] = $row;
		}
		if (isset($content)) {
			return $content;
		} else {
			return $content = array();
		}
	}
	
	// ##### No Need Now ###########################
	public static function update($data = array()) {
		if (is_array($data) && !empty($data)) {
			if(array_key_exists("column_id", $data)) {
			$query = "update ".self::$table." set title = ".utilities::$db->quote($data['title']).", description = ".utilities::$db->quote($data['description']).", updated_at = NOW() where id=".$data['column_id'];
				if(utilities::$db->query($query)) {
					$content = "Successfully updated";
					utilities::setMessage($content, "success");
					return $content;
				} else {
					$content = "Updating problem";
					utilities::setMessage($content, "error");
					return $content;
				}
			}
		}
	}
	// ##### No Need Now ###########################
	
	public static function delete($column_id) {
		$query = "delete from ".self::$table." where id=$column_id";
		if(utilities::$db->query($query)) {
			$content = "Successfully deleted";
			utilities::setMessage($content, "success");
			return $content;
		} else {
			$content = "Deleting problem";	
			utilities::setMessage($content, "error");
			return $content;	
		}
	}
	
	public static function registration($data = array()){
 		if (is_array($data) && !empty($data)) {
	 		$query = "select * from ".self::$table." where user_name=".utilities::$db->quote($data['user_name'])." or email=".utilities::$db->quote($data['email']);
	 		
	 		if(utilities::$db->query($query)->rowCount() > 0){
	 			utilities::setMessage("Username or Email already used", "error");
	 		} else{
	 			if (!isset($data['is_admin'])) {
	 				$data['is_admin'] = 0;
	 			}
	 			$query = "insert into ".self::$table." (user_name, email, password, is_admin, created_at) values (".utilities::$db->quote($data['user_name']).", ".utilities::$db->quote($data['email']).", '".sha1($data['password'])."', ".utilities::$db->quote($data['is_admin']).", NOW())";
	 			if(utilities::$db->query($query)){
	 				utilities::setMessage("registration Success ");
	 			} else{
	 				utilities::setMessage("Something wrong", "error");
	 			}
	 		}
	 	}
 	}

 	public static function login($data = array()){ 
 		$query = "select * from ".self::$table." where password='".sha1($data['password'])."' and (email=".utilities::$db->quote($data['email'])." or user_name=".utilities::$db->quote($data['email']).")";
 		if($row = utilities::$db->query($query)->fetch(PDO::FETCH_ASSOC)) {
 			$_SESSION['user_id'] = $row['id'];
 			$_SESSION['is_admin'] = $row['is_admin'];
 			utilities::setMessage("login success");
 			if (isset($_SESSION['last_uri'])) {
 				utilities::redirect($_SESSION['last_uri']);
 			} else {
 				utilities::redirect('index.php');
 			}
 		} else{
 			utilities::setMessage("Incorrect email/username or Password");
 		}	
 	}

 	public static function usernameById($user_id){ 
 		$query = "select user_name from ".self::$table." where id=".$user_id;
 		if($row = utilities::$db->query($query)->fetch(PDO::FETCH_ASSOC)) {
 			return $row['user_name'];
 		} else{
 			return "Not Found";
 		}	
 	}
}
?>