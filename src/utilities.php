<?php
namespace src;
/**
* Utilities Class
*/
use PDO;
class utilities  {
	public static $db = null;
	public static $tmp_value = array();

	public static function connect($host="localhost", $user="root", $pass="", $dbname="code") {
		$dsn = "mysql:host=".$host.";dbname=".$dbname;
		try {
		    self::$db = new PDO($dsn, $user, $pass);
		    self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (PDOException $e) {
		    echo 'Connection failed: ' . $e->getMessage();
		}


	}

	public static function destroy() {
		utilities::$db = null;
	}

	public static function dump($data = false) {
		echo "<pre>";
		var_dump($data);
		echo "</pre>";
	}

	public static function setMessage($message, $type = false) {
		if (isset($message) && $message != "") {
			if ($type == "error") {
				$_SESSION['message']['error'] = $message;
			} else {
				$_SESSION['message']['success'] = $message;
			}
		}
	}

	public static function flushMessage($type) {
		if (isset($_SESSION['message'][$type]) && $_SESSION['message'][$type] != "") {
			$message = $_SESSION['message'][$type];
			unset($_SESSION['message'][$type]);
			return $message;
		} else {
			return false;
		}
	}

	public static function searchSubArray(Array $array, $key) {
		if (isset($array[$key])) {
			return $array[$key];
		} else {
			foreach ($array as $subarray){
				if (isset($subarray[$key])) {
					return $subarray[$key];
				} else {
					return utilities::searchSubArray($subarray, $key);
				}
			} 
		}
	}

	public static function GetValueSearchSubArray(Array $search_array, $search_key, $tmp_value = array()) {
		if (is_array($search_array)) {
			foreach ($search_array as $key => $value) {
				if (isset($search_array[$search_key])) {
					self::$tmp_value[] = $search_array[$search_key];
				} else {
					self::GetValueSearchSubArray($search_array[$key], $search_key, self::$tmp_value);
				}
			}
			if (isset(self::$tmp_value)) {
				return self::$tmp_value;
			} else {
				return false;
			}
		}
	}

	public static function redirect($url) {
		if ($url != "") {
			?><script type="text/javascript">open("<?php echo $url; ?>", "_self");</script><?php
			// header("Location: $url");
		} else {
			return false;
		}
	}

	public static function logout() {
		session_destroy();
		self::redirect("index.php");
	}

	public static function send_mail($to, $subject, $message, $files = array()) {
		$uid = md5(uniqid(time()));
		$headers = "From: jbhasan@gmail.com\r\n";
		// $headers .= "Reply-To: ".$replyto."\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
		$headers .= "This is a multi-part message in MIME format.\r\n";
		$headers .= "--".$uid."\r\n";
		$headers .= "Content-type:text/html; charset=iso-8859-1\r\n";
		$headers .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
		$headers .= $message."\r\n\r\n";
		$headers .= "--".$uid."\r\n";

		foreach ($files as $key => $path) {
			$pathinfo = pathinfo($path);
			$basename = $pathinfo['basename'];

			$file = $path;
			$file_size = filesize($file);
			// $handle = fopen($file, "r");
			$handle = fopen($file,"rb");
			$content = fread($handle, $file_size);
			fclose($handle);
			$content = chunk_split(base64_encode($content));

			if ($pathinfo['extension'] == "pdf") {
				$headers .= "Content-Type: {\"application/pdf\"};\n" . " name=\"$basename\"\n"; // use different content types here
			} else {
				$headers .= "Content-Type: {\"image/jpeg\"};\n" . " name=\"$basename\"\n"; // use different content types here
			}
			$headers .= "Content-Transfer-Encoding: base64\n\n" . $content . "\n\n";
			$headers .= "Content-Disposition: attachment;\n" . " filename=\"$basename\"\n";
			
			$headers .= $content."\r\n\r\n";
	        $headers .= "--{$uid}\n";
		}
			// var_dump($content);
			// die();

		if (mail($to, $subject, "", $headers)) {
			return 'Your message has been sent.';
		} else {
			return false;
		}
	}
}
?>