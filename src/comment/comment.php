<?php
namespace src\comment;
use src\users\users;
use src\utilities;
use PDO;
class comment {
	public static $table = "comments";
	public static $tmp_id = 0;
	
	public static function index($column_id = false, $user_id = false, $snip_code_id = false) {
		$query = "select * from ".self::$table." where 1=1";
		if ($column_id) {
			$query .= " and id=$column_id";
		}
		if ($user_id) {
			$query .= " and user_id=$user_id";
		}
		if ($snip_code_id) {
			$query .= " and snip_code_id=$snip_code_id";
		}
		$result = utilities::$db->query($query);
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$content[$row['id']] = $row;
		}
		if (isset($content)) {
			return $content;
		} else {
			return $content = array();
		}
	}
	
	public static function create($data = array()) {
		if (is_array($data) && !empty($data)) {
			$query = "insert into ".self::$table." (user_id, snip_code_id, description, created_at) values (".$_SESSION['user_id'].", ".utilities::$db->quote($data['snip_code_id']).", ".utilities::$db->quote($data['description']).", NOW())";
			// echo $query;
			if(utilities::$db->query($query)) {
				$content = "Successfully stored";
				utilities::setMessage($content, "success");
				return $content;
			} else {
				$content = "Inserting problem";
				utilities::setMessage($content, "error");
				return false;
			}
		}
	}
	
	public static function update($data = array()) {
		if (is_array($data) && !empty($data)) {
			if(array_key_exists("comment_id", $data)) {				
				$query = "update ".self::$table." set description = ".utilities::$db->quote($data['description']).", updated_at = NOW() where user_id=".$_SESSION['user_id']." and id=".$data['comment_id'];
				if(utilities::$db->query($query)) {
					$content = "Successfully updated";
					utilities::setMessage($content, "success");
					return $content;
				} else {
					$content = "Updating problem";
					utilities::setMessage($content, "error");
					return false;
				}
			}
		}
	}
	
	public static function delete($column_id) {
		$query = "delete from ".self::$table." where user_id=".$_SESSION['user_id']." and id=$column_id";
		if(utilities::$db->query($query)) {
			$content = "Successfully deleted";
			utilities::setMessage($content, "error");
			return $content;
		} else {
			$content = "Deleting problem";	
			utilities::setMessage($content, "error");
			return $content;	
		}
	}

	public static function code_view($data) {
		$data = html_entity_decode($data);
		$tmp_data = explode("<<\r\n", $data);
		for ($tmp=1; $tmp < count($tmp_data); $tmp++) {
			self::$tmp_id++;
			$data = self::str_replace2("<<\r\n", '<div class="code" id="code'.self::$tmp_id.'">', $data, 1);
		}
		$tmp_data = explode("<<", $data);
		for ($tmp=1; $tmp < count($tmp_data); $tmp++) {
			self::$tmp_id++;
			$data = self::str_replace2("<<", '<div class="code" id="code'.self::$tmp_id.'">', $data, 1);
		}
		$data = str_replace(">>", '</div>', $data);
		$data = str_replace('\r\n', "\r\n", $data);
		return trim($data);
	}

	public static function str_replace2($find, $replacement, $subject, $limit = 0, &$count = 0){
		if ($limit == 0)
	    	return str_replace($find, $replacement, $subject, $count);
		$ptn = '/' . preg_quote($find,'/') . '/';
		return preg_replace($ptn, $replacement, $subject, $limit, $count);
	}

	public static function comment_view($comment_list = array(), $snip_code_id) {
		foreach ($comment_list as $comment_id => $value_in_array) {
			?>
			<div id="commentRow_<?php echo $snip_code_id; ?>_<?php echo $comment_id; ?>" class="row">
				<div class="col-md-1 text-right">
					<?php
					$photo = glob("imgs/profile_photo/".$value_in_array['user_id'].".*");
					if ($photo) {
						$photo_path = $photo[0];
					} else {
						$photo_path = "imgs/picture.png";
					}
					?>
					<img src="<?php echo $photo_path; ?>" class="img-rounded" style="border:solid 1px black; width:50px;" />
				</div>
				<div class="col-md-11">
					<strong><?php echo users::usernameById($value_in_array['user_id']); ?></strong>
					<span><?php echo $value_in_array['description']; ?></span><br>
					<small style="color:gray;">
						<?php
						if (isset($_SESSION['user_id']) && $_SESSION['user_id'] == $value_in_array['user_id']) {
							?>
								<span class="glyphicon glyphicon-pencil pull-left" style="cursor:pointer; word-spacing: -8px; padding-right:10px;" onclick="commentBox(<?php echo $snip_code_id; ?>, <?php echo $value_in_array['id']; ?>, '<?php echo $value_in_array['description']; ?>')"> Edit</span>
								<form class="pull-left" action="" method="post" name="del_comment_<?php echo $comment_id; ?>" id="del_comment_<?php echo $comment_id; ?>">
									<input type="hidden" name="del_comment_id" value="<?php echo $comment_id; ?>">
									<button type="submit" class="hidden" name="some" value="<?php echo $comment_id; ?>"></button>
									<div class="glyphicon glyphicon-remove" onclick="del_comment_<?php echo $comment_id; ?>.submit();" style="cursor:pointer; word-spacing: -8px;"> Delete</div>
								</form><br>
							<?php
						}
						?>
						<span>Commented at <?php echo $value_in_array['created_at']; ?></span>
					</small>
				</div>
			</div>
			<div class="clearfix"><hr></div>
			<?php
		}
	}
}
?>