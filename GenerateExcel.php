<?php
session_start();
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
date_default_timezone_set('asia/Dhaka');

use src\bitm\SEIP108349\student\student;
use src\bitm\SEIP108349\utilities;

require_once("vendor/autoload.php");

$phpexcel_path = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'bitm_php/Md. Hasan Sayeed_108349_B13_L8-MyTry'.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'phpoffice'.DIRECTORY_SEPARATOR.'phpexcel'.DIRECTORY_SEPARATOR.'Classes'.DIRECTORY_SEPARATOR.'PHPExcel.php';
if (file_exists($phpexcel_path)) {
	include_once($phpexcel_path);
}
// use vendor\phpoffice\phpexcel\Classes\PHPExcel;
// use vendor\phpoffice\phpexcel\Classes\PHPExcel;
// use vendor\PHPExcelClasses\PHPExcel;

if (isset($_POST['data_list']) && $_POST['data_list'] != "") {
	$encode_data_in_array = $_POST['data_list'];
	$objPHPExcel = new PHPExcel();
	
	/** Include PHPExcel */
	if($encode_data_in_array != "") {
		$data_list = unserialize(base64_decode($encode_data_in_array));

		// Create new PHPExcel object
		// echo date('h:i:s A') , " Create new PHPExcel object" , EOL;

		// Set document properties
		// echo date('H:i:s') , " Set document properties" , EOL;
		$objPHPExcel->getProperties()->setCreator("HasanSayeed")
									 ->setLastModifiedBy("Hasan Sayeed")
									 ->setTitle("For BITM")
									 ->setSubject("Office 2007 XLSX Document")
									 ->setDescription("Test excel using PHP")
									 ->setKeywords("office 2007 openxml php")
									 ->setCategory("BITM TEST");


		// Add some data, we will use printing features
		// echo date('H:i:s') , " Add some data" , EOL;
		//  DB connection
		// echo "This is working";
		if (isset($data_list) && !empty($data_list)) {
			// echo "<pre>";
			// var_dump($data_list);
			// echo "</pre>";
				
			// header ===============

			// $objPHPExcel->getDefaultStyle()->getFont()
			//     ->setName('Calibri')
			//     ->setSize(11);
			$column_start = 'A';
			$column_end = 'A';
			for ($i=1; $i < count($data_list[1]); $i++) { 
				$column_end++;
			}
			$column_end;
			$objPHPExcel->getActiveSheet()->getStyle($column_start."1:".$column_end."1")->getFont()->setBold(true)
			    ->setName('Calibri')
			    ->setSize(11);

			$objPHPExcel->getActiveSheet()->getStyle($column_start.":".$column_end)->getFont()
			    ->setName('Calibri')
			    ->setSize(11);

			// Set Cell Alignment
			$objPHPExcel->getActiveSheet()->getStyle($column_start."1:".$column_end."1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle($column_start.":".$column_end)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			// $objPHPExcel->getActiveSheet()->getStyle('C:D')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

			$objPHPExcel->getActiveSheet()->getStyle($column_start.":".$column_end)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

			// Set Column Wrap
			$objPHPExcel->getActiveSheet()->getStyle($column_start.":".$column_end)->getAlignment()->setWrapText(true);

			// Set Row Repeat For Print
			$objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1); 

			$tmp_column_start = $column_start;
			foreach ($data_list[1] as $key => $value) {
				$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue($tmp_column_start.'1', $key);
				
				// Set Row Width
				// $objPHPExcel->getActiveSheet()->getColumnDimension($tmp_column_start)->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension($tmp_column_start)->setAutoSize(true);
				
				$tmp_column_start++;
			}
			$sl = 2;

			foreach ($data_list as $data_id => $value_in_array) {
				$tmp_column_start = $column_start;
				$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A'.$sl, $sl-1);
				foreach ($data_list[$data_id] as $key => $value) {
					$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue($tmp_column_start.$sl, $value);
					$tmp_column_start++;
				}
				$sl++;
			}
		
			// Set Row Height
			$objPHPExcel->getActiveSheet()->getRowDimension($column_start.":".$column_end)->setRowHeight(36);

			$styleArray = array(
			          'borders' => array(
			              'allborders' => array(
			                  'style' => PHPExcel_Style_Border::BORDER_THIN
			              )
			          )
			      );
			$objPHPExcel->getActiveSheet()->getStyle($column_start."1:".$column_end.($sl-1))->applyFromArray($styleArray);
			unset($styleArray);

			// Margin Here
			$sheet = $objPHPExcel->getActiveSheet();
			$pageMargins = $sheet->getPageMargins();
			$pageMargins->setTop('0.40');
			$pageMargins->setBottom('0.60');
			$pageMargins->setLeft('0.70');
			$pageMargins->setRight('0.70');

			// Set header and footer. When no different headers for odd/even are used, odd header is assumed.
			// echo date('H:i:s') , " Set header/footer" , EOL;
			$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddHeader('');
			$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&R Page &P of &N');

			// Set page orientation and size
			$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LEGAL);

			// Rename worksheet
			$objPHPExcel->getActiveSheet()->setTitle('TEST EXCEL SHEET');

			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			$callStartTime = microtime(true);

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$file_name = "BITM TEST EXCEL ".date('d-m-Y');
			if (!file_exists('temp_excel')) {
				mkdir('temp_excel', 0777, true);
			}
			$objWriter->save('temp_excel/'.$file_name.'.xls');
			$callEndTime = microtime(true);
			$callTime = $callEndTime - $callStartTime;
			$file_path = 'temp_excel/'.$file_name.'.xls';
			echo "<div style='font-family:Arail;font-size:35px;text-align:center;margin-top:20px;'><a href='temp_excel/".$file_name.".xls' style='color:green;'>Download : ".$file_name."</a></div>";
			// header('Content-Disposition: attachment;filename="'.$file_path.'"');
			// die();
		}
	}
} else {
	?><script type="text/javascript">open("index.php", "_self");</script><?php
}
?>