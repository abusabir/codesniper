function commentBox(boxId, commentId = "", prevText = "") {
	$("#commentRow_"+boxId+"_0").remove();
	if (prevText != "") {
		$( "#commentBox_"+boxId ).append( "<div id='commentRow_"+boxId+"_0' class='row'><div class='col-md-1 text-right'></div><div class='col-md-11'><form action='' method='post'><textarea id='text_"+boxId+"_0' name='text_"+boxId+"_0' class='form-control'></textarea><br/><input type='hidden' value='"+commentId+"' name='comment_id'><input type='submit' class='btn btn-success' name='btnUpdateComment' value='Update Comment'></form></div></div>");
		$("#commentBox_"+boxId).addClass("panel-body");
		$("#text_"+boxId+"_0").val(prevText);
	} else {
		$( "#commentBox_"+boxId ).append( "<div id='commentRow_"+boxId+"_0' class='row'><div class='col-md-1 text-right'></div><div class='col-md-11'><form action='' method='post'><textarea id='text_"+boxId+"_0' name='text_"+boxId+"_0' class='form-control'></textarea><br/><input type='submit' class='btn btn-success' name='btnAddComment' value='Add Comment'></form></div></div>");
	};
	$("#text_"+boxId+"_0").focus();
}