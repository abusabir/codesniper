<?php
session_start();
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
date_default_timezone_set('asia/Dhaka');

use src\bitm\SEIP108349\student\student;
use src\bitm\SEIP108349\utilities;

require_once("vendor/autoload.php");

$mpdf_path = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'bitm_php/Md. Hasan Sayeed_108349_B13_L8-MyTry'.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'mpdf'.DIRECTORY_SEPARATOR.'mpdf'.DIRECTORY_SEPARATOR.'mpdf.php';
if (file_exists($mpdf_path)) {
	include_once($mpdf_path);
}

if (isset($_POST['data_list']) && $_POST['data_list'] != "") {
	$encode_data_in_array = $_POST['data_list'];
	if($encode_data_in_array != "") {
		$data_list = unserialize(base64_decode($encode_data_in_array));

		$trs = "";
		$head_trs = "";
		foreach ($data_list as $data_id => $value_in_array) {
			$head_trs .="<tr>";
			foreach ($data_list[$data_id] as $key => $value) {
				$head_trs .="<td>".$key."</td>";
			}
			$head_trs .="</tr>";
			break;
		}

		$sl=1;
		foreach ($data_list as $data_id => $value_in_array) {
			$trs .="<tr>";
			foreach ($data_list[$data_id] as $key => $value) {
				$trs .="<td>".$value."</td>";
			}
			$trs .="</tr>";
			$sl++;
		}  

$html = <<<BITM
<!DOCTYPE html>
<html>
    <head>
        <title>List of Books</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            .delete{
                border:1px solid red;
            }
        </style>
    
    </head>
    
    <body>
        <h1>Data List</h1>
        <table border="1">
            <thead>
                echo $head_trs;
            </thead>
            <tbody>
              echo $trs;        
        </tbody>
        </table>
    </body>
</html>
BITM;
		$mpdf = new mPDF('c');
		$mpdf->WriteHTML($html);
		$mpdf->Output();
		exit;
	}
} else {
	?><script type="text/javascript">open("index.php", "_self");</script><?php
}
?>